<?php

namespace Drupal\entity_bundle_field\Plugin\Field\FieldType;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TraversableTypedDataInterface;
use Drupal\Core\TypedData\TypedDataInterface;

/**
 * Defines the 'entity_bundle' field type.
 *
 * @FieldType(
 *   id = "entity_bundle",
 *   label = @Translation("Entity bundle reference"),
 *   description = @Translation("Render the referenced entity."),
 *   category = "reference",
 *   default_formatter = "entity_bundle_formatter",
 *   cardinality = 1,
 *   default_widget = "entity_bundle_widget",
 *   field_types = {
 *     "entity_bundle"
 *   }
 * )
 */
class EntityBundle extends FieldItemBase {

  /**
   * Constructs a TypedData object given its definition and context.
   *
   * @param \Drupal\Core\TypedData\DataDefinitionInterface $definition
   *   The data definition.
   * @param string $name
   *   (optional) The name of the created property, or NULL if it is the root
   *   of a typed data tree. Defaults to NULL.
   * @param \Drupal\Core\TypedData\TypedDataInterface|null $parent
   *   (optional) The parent object of the data property, or NULL if it is the
   *   root of a typed data tree. Defaults to NULL.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface|null $entityTypeManager
   *   (optional) The entity type manager.
   *
   * @see \Drupal\Core\TypedData\TypedDataManager::create()
   */
  public function __construct(
    DataDefinitionInterface $definition,
    $name = NULL,
    ?TypedDataInterface $parent = NULL,
    protected readonly ?EntityTypeManagerInterface $entityTypeManager = NULL,
  ) {
    $this->definition = $definition;
    $this->parent = $parent;
    $this->name = $name;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance($definition, $name = NULL, ?TraversableTypedDataInterface $parent = NULL) {
    return new static($definition, $name, $parent, \Drupal::entityTypeManager());
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['bundles'] = DataDefinition::create('string')
      ->setLabel(t('Select option'))
      ->setRequired(FALSE);
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'bundles' => [
        'type' => 'varchar',
        'length' => 255,
      ],
    ];

    return [
      'columns' => $columns,
      'indexes' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('bundles')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * Define el formulario del campo.
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);
    $bundlable_entities = [];
    // Get the list of all entity types.
    $entity_types = \Drupal::entityTypeManager()->getDefinitions();

    foreach ($entity_types as $entity_type_id => $entity_type) {
      // Check if the entity type is "bundlable".
      $bundle = $entity_type->getBundleEntityType();
      if (!empty($bundle)) {
        // Add it to the list.
        $bundlable_entities[$entity_type_id] = $entity_type->getLabel();
      }
    }

    $element['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select entity bundle type'),
      '#description' => $this->t("Select entity bundle type that will support the field."),
      '#options' => $bundlable_entities,
      '#default_value' => $this->getSetting('entity_type'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'entity_type' => '',
    ];
  }

}
