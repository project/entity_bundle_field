<?php

namespace Drupal\entity_bundle_field\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'entity_bundle_widget' widget.
 *
 * @FieldWidget(
 *   id = "entity_bundle_widget",
 *   label = @Translation("Entity Bundle Widget"),
 *   field_types = {
 *     "entity_bundle"
 *   }
 * )
 */
class EntityBundleWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $entity_type = !empty($items->getFieldDefinition()->getSettings()['entity_type']) ? $items->getFieldDefinition()->getSettings()['entity_type'] : NULL;
    if (!empty($entity_type)) {
      $definition = $this->entityTypeManager->getDefinition($entity_type);
      $bundles = $this->entityTypeManager->getStorage($definition->getBundleEntityType())->loadMultiple();
      $options = [];
      foreach ($bundles as $bundle) {
        $bundle_name = $bundle->label();
        $bundle_id = $bundle->id();
        $options[$bundle_id] = $bundle_name;
      }
      $element['bundles'] = $element + [
        '#type' => 'select',
        '#empty_option' => $this->t('- None -'),
        '#default_value' => $items[$delta]->getValue(),
        '#options' => $options,
      ];
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $new_values = [];

    foreach ($values as $item) {
      if (isset($item['bundles'])) {
        $new_values['value'] = $item['bundles'];
      }
    }
    return $new_values["value"];
  }

  /**
   * Return an array of items.
   *
   * @return array
   *   Return an array of selections.
   */
  public function getSelections():array {
    $selections = [];
    foreach ($this as $item) {
      $selections[] = $item->value;
    }
    return $selections;
  }

}
