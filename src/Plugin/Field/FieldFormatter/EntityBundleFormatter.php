<?php

namespace Drupal\entity_bundle_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'entity_bundle_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_bundle_formatter",
 *   label = @Translation("Entity Bundle Formatter"),
 *   field_types = {
 *     "entity_bundle"
 *   }
 * )
 */
class EntityBundleFormatter extends FormatterBase {

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entityTypeBundleInfo
   *   The entity type bundle info.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly EntityTypeBundleInfo $entityTypeBundleInfo,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->fieldDefinition = $field_definition;
    $this->settings = $settings;
    $this->label = $label;
    $this->viewMode = $view_mode;
    $this->thirdPartySettings = $third_party_settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $entityTypeId = !empty($items->getFieldDefinition()->getSettings()['entity_type']) ? $items->getFieldDefinition()->getSettings()['entity_type'] : NULL;
    if ($entityTypeId) {
      $entityType = $this->entityTypeManager->getDefinition($entityTypeId);
      if ($entityType) {
        $bundleInfo = $this->entityTypeBundleInfo->getBundleInfo($entityTypeId);
        foreach ($items as $delta => $item) {
          $label = $this->t('Undefined');
          if (isset($bundleInfo[$item->bundles]['label'])) {
            $label = $bundleInfo[$item->bundles]['label'];
          }
          // The text value has no text format assigned to it, so the user input
          // should equal the output, including newlines.
          $elements[$delta] = [
            '#type' => 'inline_template',
            '#template' => '{{ value|nl2br }}',
            '#context' => ['value' => $label],
          ];
        }
      }
    }

    return $elements;
  }

}
