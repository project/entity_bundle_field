# Entity Bundle Field

Entity Bundle Field is a lightweight and simple module that provides a field
type that allows referencing Content Types and Vocabularies.

**Features**
If you need to render a content type or vocabulary on screen, or simply need
to send the value of such an entity to the front end of a decoupled Drupal,
this simple field can meet your expectations.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/entity_bundle_field).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/entity_bundle_field).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Add a field of type "Entity Bundle Reference" to the desired entity
(Node, Term...). Configure the type of entity that the field will show.
Finally, create a content of that entity and you will be able to reference
the entity bundle you previously configured.


## Maintainers

- Juan Delgado - [juandels3](https://www.drupal.org/u/juandels3)
